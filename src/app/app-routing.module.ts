import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WeatherOverviewComponent } from './weather-overview/weather-overview.component';


const routes: Routes = [
  { path: '', component: WeatherOverviewComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
