import { Component, OnInit } from '@angular/core';
import { Forecast } from './forecast';
import { WeatherServiceService } from './service/weather-service.service';

@Component({
  selector: 'app-weather-overview',
  templateUrl: './weather-overview.component.html',
  styleUrls: ['./weather-overview.component.css']
})
export class WeatherOverviewComponent implements OnInit {

  private requestedCities = ['Katowice', 'Warszawa', 'Wrocław', 'Gdańsk', 'Poznań'];
  forecasts: Forecast[] = [];

  constructor(private weatherService: WeatherServiceService) { }

  ngOnInit() {
    this.weatherService.fetchForecasts(this.requestedCities)
      .subscribe(
        (forecasts) => this.forecasts = forecasts,
        (error) => console.error(error)
      );
  }

}
