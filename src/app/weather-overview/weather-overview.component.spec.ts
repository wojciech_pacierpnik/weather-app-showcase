import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WeatherOverviewComponent } from './weather-overview.component';
import { WeatherServiceService } from './service/weather-service.service';
import { of } from 'rxjs';

describe('WeatherOverviewComponent', () => {
  let component: WeatherOverviewComponent;
  let fixture: ComponentFixture<WeatherOverviewComponent>;

  beforeEach(async(() => {
    const weatherServiceStub = {
      fetchForecasts: (cities) => of([])
    };

    TestBed.configureTestingModule({
      declarations: [ WeatherOverviewComponent ],
      providers: [
        { provide: WeatherServiceService, useValue: weatherServiceStub }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeatherOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should communicate fetch failure', () => {
    fixture.detectChanges();
    const pageElement: HTMLElement = fixture.nativeElement;
    const alert = pageElement.querySelector('.alert');
    expect(alert.textContent).toContain('Nie udało się pobrać danych');
  });
});
