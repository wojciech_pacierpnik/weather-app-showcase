import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Forecast } from '../forecast';

const MOCKED_FORECASTS: Forecast[] = [
  { city: 'Katowice', temperature: 20, rainChances: 33, lastUpdate: new Date() },
  { city: 'Warszawa', temperature: 18, rainChances: 20 },
  { city: 'Wrocław', temperature: 21, rainChances: 17 },
  { city: 'Gdańsk', temperature: 16, rainChances: 59 },
  { city: 'Poznań', temperature: 19, rainChances: 15 }
];

@Injectable({
  providedIn: 'root'
})
export class WeatherServiceService {

  constructor() { }

  fetchForecasts(cities: string[]): Observable<Forecast[]> {
    return of(MOCKED_FORECASTS);
  }
}
