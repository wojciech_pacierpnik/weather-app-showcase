export interface Forecast {
  city: string;
  temperature: number;
  rainChances: number;
  lastUpdate?: Date;
}
