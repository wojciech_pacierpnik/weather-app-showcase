// import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WeatherOverviewComponent } from './weather-overview/weather-overview.component';

@NgModule({
  declarations: [
    AppComponent,
    WeatherOverviewComponent
  ],
  imports: [
    // CommonModule,
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
